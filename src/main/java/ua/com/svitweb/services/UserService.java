package ua.com.svitweb.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.svitweb.Exceptions.UserNotFoundException;
import ua.com.svitweb.dao.UserRepository;
import ua.com.svitweb.dto.UserAuthorized;
import ua.com.svitweb.entity.UserEntity;
import ua.com.svitweb.mapper.UserMapper;
import ua.com.svitweb.model.User;
import ua.com.svitweb.util.MyHasher;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Transactional
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserMapper userMapper;

    ConcurrentHashMap activeUsers;

    public User writeUser(User user) {
        UserEntity userEntity;
        if (user.getId() != null) {
            userEntity = userRepository.findById(user.getId()).get();
            if (user.getName() != null) {
                userEntity.setName(user.getName());
            }
            if (user.getEmail() != null) {
                userEntity.setEmail(user.getEmail());
            }
            if (user.getPassword() != null) {
                userEntity.setPassword(user.getPassword());
            }
        } else {
            userEntity = userMapper.convert(user);
        }

        return userMapper.convert(userRepository.save(userEntity));
    }


    public List<User> users() {
        List<UserEntity> ueList = userRepository.findAll();
        List<User> users = new ArrayList<>();
        for (UserEntity user : ueList) {
            users.add(userMapper.convert(user));
        }
        return users;
    }

    public User getUserById(Integer id) {
//        UserEntity userEntity=null;
//        try {
//            userEntity = userRepository.findById(id).get();
//        } catch (NoSuchElementException e) {
//            e.printStackTrace();
//        }
        UserEntity userEntity = userRepository.findById(id).get();
        return userMapper.convert(userEntity);
    }

    public List<User> getAllUsersByName(String username) {
        List<UserEntity> entitiesByName = userRepository.findByName(username);
        if (entitiesByName.size() == 0) {
            return null;
        }
        List<User> usersByName = new ArrayList<>();
        for (UserEntity ue : entitiesByName) {
            usersByName.add(userMapper.convert(ue));
        }
        return usersByName;
    }

    public boolean deleteUser(Integer userId) {
        userRepository.deleteById(userId);
        return true;
    }
  /*  public boolean deleteAllUsersByName(String name) {
        userRepository.deleteAllByName(name);
        return true;
    }*/

    public String authorize(String email, String pass) throws UserNotFoundException{
        User user = userMapper.convert(userRepository.findByEmail(email));
        if (user.getPassword().equals(MyHasher.hash(pass))){
            String uuid = UUID.randomUUID().toString();
            UserAuthorized userAuthorized = userMapper.convertToAuthorized(user);
            activeUsers.put(uuid,user);
            return uuid;
        }
        else throw new UserNotFoundException(email);

    }
}