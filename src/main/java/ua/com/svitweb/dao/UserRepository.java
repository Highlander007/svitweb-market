package ua.com.svitweb.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.svitweb.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    UserEntity findByEmail(String email);

    List<UserEntity> findByName(String name);

//    void deleteAllByName(String name);

//    @Query("delete from Users where name = "name"")
//    void findByEmailReturnStream(@Param("name") String name);

    // custom query example and return a stream
/*   @Query("select * from Users where name = "name"")
    Stream<UserEntity> findByEmailReturnStream(@Param("name") String name);*/


}
