package ua.com.svitweb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ua.com.svitweb.dao.UserRepository;
//import ua.com.svitweb.services.UserService;

@SpringBootApplication
public class MarketServer {


    @Autowired
    UserRepository userRepository;

    private static final Logger log = LoggerFactory.getLogger(MarketServer.class);

    public static void main(String[] args) {
        SpringApplication.run(MarketServer.class, args);

    }


    //	}
//    @Bean
//    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
//        return args -> {
//
//            System.out.println("Let's inspect the beans provided by Spring Boot:");
//
//            String[] beanNames = ctx.getBeanDefinitionNames();
//            Arrays.sort(beanNames);
//            for (String beanName : beanNames) {
//                System.out.println(beanName);
//            }
//
//        };
//    }

}