package ua.com.svitweb.util;

import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.com.svitweb.Exceptions.UserNotFoundException;

import java.util.Optional;

@ControllerAdvice
@RequestMapping(produces = "application/error+json")
public class UserControllerAdvice {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity  notFoundException(final UserNotFoundException e) {

        return error(e, HttpStatus.NOT_FOUND, e.toString());

    }

    private ResponseEntity error(final Exception exception, final HttpStatus httpStatus, final String logRef) {

        final String message = Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());

        return new ResponseEntity< >(new VndErrors(logRef, message), httpStatus);

    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity assertionException(final IllegalArgumentException e) {

        return error(e, HttpStatus.NOT_FOUND, e.getLocalizedMessage());

    }

}