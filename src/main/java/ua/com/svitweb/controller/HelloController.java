package ua.com.svitweb.controller;

import ua.com.svitweb.util.LoginAuthentificator;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(@RequestParam(value = "name", required = false) String name) {
        return "Greetings from Spring Boot " + (name!=null?(", "+ name):"") + "!";
    }

    @PostMapping("/login")
    public boolean add(@RequestParam(value = "login", required = false) String login,
                       @RequestParam(value = "pss", required = false) String parol) {
        return LoginAuthentificator.verify(login, parol);
    }

    @PostMapping("/emptyPost")
    public void emptyPost() {

    }
}