package ua.com.svitweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import ua.com.svitweb.Exceptions.UserNotFoundException;
import ua.com.svitweb.dto.CreateUserDTO;
import ua.com.svitweb.dto.UserDTO;
import ua.com.svitweb.mapper.UserMapper;
import ua.com.svitweb.model.User;
import ua.com.svitweb.services.UserService;
import ua.com.svitweb.util.LoginAuthentificator;

@RestController
@RequestMapping("/api/v1/users/")
public class UserController {

    private static AtomicLong counter = new AtomicLong();

    @Autowired
    private UserService userService;

    @Autowired
    UserMapper userMapper;

    @PostMapping("/login")
    public String authentification(@RequestParam(value = "login") String login,@RequestParam(value = "passw") String number) throws UserNotFoundException {
        return userService.authorize(login, number);
    }

    @GetMapping("/user/{name}")
    public List<User> getUserByName(@PathVariable String name) {
        return userService.getAllUsersByName(name);
    }

    @GetMapping(value = "/user/id/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUserById(@PathVariable Integer id) {
        return userService.getUserById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/user")
    public User addUser(@RequestBody CreateUserDTO createUserDTO ) {
        User user =  userMapper.convert(createUserDTO);
        return userService.writeUser(user);
    }

    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    public User editUser(@RequestBody UserDTO userDTO ) {
        User user = userMapper.convert(userDTO);
        return userService.writeUser(user);
    }

    @GetMapping("/")
    public List<User> getAllUsers() {
        return userService.users();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/user/delete/id/{id}")
    public boolean deleteUserById(@PathVariable Integer id) {
        return userService.deleteUser(id);
    }

   /* @DeleteMapping("/user/delete/name/{name}")
    public boolean deleteAllUsersByName(@PathVariable String name){return userService.deleteAllUsersByName(name);}*/
}
