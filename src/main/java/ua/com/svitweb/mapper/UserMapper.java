package ua.com.svitweb.mapper;


import org.mapstruct.Mapper;
import ua.com.svitweb.dto.CreateUserDTO;
import ua.com.svitweb.dto.UserAuthorized;
import ua.com.svitweb.dto.UserDTO;
import ua.com.svitweb.entity.UserEntity;
import ua.com.svitweb.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User convert(UserDTO userDTO);
    User convert(CreateUserDTO createUserDTO);
    User convert(UserEntity userEntity);
    UserAuthorized convertToAuthorized(User user);
    UserEntity convert(User user);
}
