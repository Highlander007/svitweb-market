package ua.com.svitweb.Exceptions;

public class UserNotFoundException extends Exception {
    public UserNotFoundException(int id){
        super("User with id:"+ id+" Not Found.");
    }
    public UserNotFoundException(String email){
        super("User with email:"+ email+" is Not Registered.");
    }
}
