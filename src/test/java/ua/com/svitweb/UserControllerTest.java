package ua.com.svitweb;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.stream.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.com.svitweb.controller.UserController;
import ua.com.svitweb.dao.UserRepository;
import ua.com.svitweb.model.User;

import java.io.UnsupportedEncodingException;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
//@AutoConfigureMockMvc
//@WebAppConfiguration
//@ContextConfiguration
public class UserControllerTest {

    @Mock
    UserController userController;

    @Autowired
    private WebApplicationContext wac;

//  @Autowired
//  MockHttpSession session;

    @Autowired
    ObjectMapper objectMapper;
    JacksonTester<User> jsonUser;

    private MockMvc mockMvc;
    private int id;
    private String body(MvcResult result) throws UnsupportedEncodingException {
        return result.getResponse().getContentAsString();
    }

    @Before
    public void setup() throws Exception{
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        JacksonTester.initFields(this, objectMapper);
//        String json1 = "{\n " +
//                "        \"id\": \"11\",\n" +
//                "        \"name\": \"oosdfho\",\n" +
//                "        \"password\": \"parolikus2000\",\n" +
//                "        \"email\": \"pedro@poshta.ye\"\n" +
//                "    }";
//
//        MvcResult result = mockMvc.perform(post("/api/v1/users/user")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(json1)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.name", is("oosdfho")))
//                .andExpect(jsonPath("$.email", is("pedro@poshta.ye")))
//                .andExpect(jsonPath("$.password", is("parolikus2000")))
//                .andExpect(status().isCreated())
//                .andReturn();
//
//        String body = body(result);
//        User u2 = jsonUser.parseObject(body);
//        id=u2.getId();
    }

    @After
    public void clean(){

    }

    @Test
    public void addUserTest() throws Exception {
        String json = "{\n " +
                "        \"name\": \"PedroTheFirst\",\n" +
                "        \"password\": \"password001\",\n" +
                "        \"email\": \"pedro001@poshta.ye\"\n" +
                "    }";
        MvcResult result = mockMvc.perform(post("/api/v1/users/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name",is("PedroTheFirst")))
                .andExpect(jsonPath("$.password",is("password001")))
                .andExpect(jsonPath("$.email",is("pedro001@poshta.ye")))
                .andExpect(status().isCreated()).andReturn();
        String body = body(result);
        User u2 = jsonUser.parseObject(body);
        id=u2.getId();

    }

    @Test
    public void editUserTest() throws Exception {

//        CreateUserDTO user = new CreateUserDTO();
//        user.setEmail("oosdfho");
//        user.setEmail("parolikus2000");
//        session.setAttribute("sessionParm",user);

//        String json1 = "{\n " +
//                "        \"id\": \"11\",\n" +
//                "        \"name\": \"ostap\",\n" +
//                "        \"password\": \"parolikus2000\",\n" +
//                "        \"email\": \"pedro@poshta.ye\"\n" +
//                "    }";
//        MvcResult result = mockMvc.perform(post("/api/v1/users/user")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(json1)
////                .session(session)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.name", is("ostap")))
//                .andExpect(jsonPath("$.email", is("pedro@poshta.ye")))
//                .andExpect(jsonPath("$.password", is("parolikus2000")))
//                .andExpect(status().isCreated())
//                .andReturn();
//
//        String body = body(result);
//        User u2 = jsonUser.parseObject(body);
//        id=u2.getId();
        addUserTest();

        String json2 = "{\n " +
                "        \"id\": \""+id+"\",\n" +
                "        \"name\": \"Pedro\",\n" +
                "        \"password\": \"parolikus3000\",\n" +
                "        \"email\": \"pedro365@poshta.ye\"\n" +
                "    }";
        mockMvc.perform(put("/api/v1/users/user")
                .contentType(MediaType.APPLICATION_JSON).content(json2)
                .accept(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.id",is(id)))
                .andExpect(jsonPath("$.name",is("Pedro")))
                .andExpect(jsonPath("$.email",is("pedro365@poshta.ye")))
                .andExpect(jsonPath("$.password",is("parolikus3000")))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllUsersTest() throws Exception {
        addUserTest();
        MvcResult result = mockMvc.perform(get("/api/v1/users/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        // this uses a TypeReference to inform Jackson about the Lists's generic type
        List<User> usersList = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<User>>(){});
        int size = usersList.size();
        System.out.println("usersList size:"+size);
        List<User> sorted = usersList.stream().sorted((o1, o2)->o1.getId()
                .compareTo(o2.getId()))
                .collect(Collectors.toList());
        for (User user : sorted) {
            System.out.println("id:"+user.getId()+" name:"+user.getName()+" email:"+user.getEmail()+" password:"+user.getPassword());
        }
    }

    @Test
    public void getUserByNameTest() throws Exception {
        mockMvc.perform(get("/api/v1/users/user/PedroTheFirst"))
//                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void getUserByIdTest() throws Exception {
        addUserTest();
        mockMvc.perform(get("/api/v1/users/user/id/"+id))
//                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
        deleteUserByIdTest();
    }

    @Test
    public void deleteUserByIdTest() throws Exception {
                String json1 = "{\n " +
                "        \"name\": \"userToDelete\",\n" +
                "        \"password\": \"passwordToPass\",\n" +
                "        \"email\": \"email@toMail.ya\"\n" +
                "    }";
        int delId;
        MvcResult result = mockMvc.perform(post("/api/v1/users/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json1)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("userToDelete")))
                .andExpect(jsonPath("$.email", is("email@toMail.ya")))
                .andExpect(jsonPath("$.password", is("passwordToPass")))
                .andExpect(status().isCreated())
                .andReturn();

        String body = body(result);
        User u2 = jsonUser.parseObject(body);
        delId=u2.getId();

        mockMvc.perform(delete("/api/v1/users/user/delete/id/"+delId))
//                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent());
    }
}
