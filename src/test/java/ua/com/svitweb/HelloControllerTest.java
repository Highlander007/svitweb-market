package ua.com.svitweb;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.http.MediaType;
import ua.com.svitweb.services.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HelloControllerTest {

    @Autowired
    private MockMvc mvc;


    @MockBean
    UserService userService;

    @Test
    public void getHello() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Greetings from Spring Boot !")));
    }

    @Test
    public void loginTest2() throws Exception {
      mvc.perform(post("/login")
                .param("login", "admin")
                .param("pss", "87654321")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
//        mvc.perform(MockMvcRequestBuilders.post("/login").accept(MediaType.APPLICATION_JSON))

//                .andExpect(model().attribute("login", is("admin")));
               // .andExpect(model().attribute("pss", is(87654321)));
    }
}