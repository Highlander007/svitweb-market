package ua.com.svitweb;

import static org.junit.Assert.*;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ua.com.svitweb.services.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoginTest {

    @LocalServerPort
    private int port;

    @MockBean
    UserService userService;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void loginTest() throws Exception{
        String url = "http://localhost:" + port + "/login";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("login", "admin");
        map.add("pss", "87654321");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        ResponseEntity<Boolean> response = template.postForEntity( url, request , Boolean.class );
        assertTrue(response.getBody());
    }

    @Test
    public void emptyPostTest() throws Exception{
        String url = "http://localhost:" + port + "/emptyPost";

        HttpHeaders headers = new HttpHeaders();

        HttpEntity request = new HttpEntity(headers);

        ResponseEntity response = template.postForEntity( url, request, null );
        assertTrue(HttpStatus.OK==response.getStatusCode());

    }
}
